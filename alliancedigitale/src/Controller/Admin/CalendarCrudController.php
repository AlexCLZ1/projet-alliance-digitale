<?php

namespace App\Controller\Admin;

use App\Entity\Calendar;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CalendarCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Calendar::class;
    }

    public function configureCrud(Crud $crud): Crud{
        return $crud
        ->setEntityLabelInPlural('Tâches')
        ->setEntityLabelInSingular('Tâche')

        ->setPageTitle("index", "Administration des Tâches");
    }
    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
            ->hideOnForm(),
            TextField::new('title', "Titre"),
            TextField::new('description'),
            DateTimeField::new('start', "Début"),
            DateTimeField::new('end', "Fin"),
            ChoiceField::new('background_color', "Couleur de la tâche")
           ->setChoices([
                    'Tâche à faire (rouge)' => '#dc3545',
                    'Tâche effectuée (verte)' => '#28a745',
                    'Tâche récurente (noir)' => '#000000',
            ])
            ->allowMultipleChoices(false)
            ->renderExpanded(true),
            BooleanField::new('all_day', "Toute la journée?"),
            BooleanField::new('is_validated', "Est-elle effectué?"),
    ];
    }
    
}
