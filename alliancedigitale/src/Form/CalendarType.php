<?php

namespace App\Form;

use App\Entity\Calendar;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class CalendarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // ->add('title')
            // ->add('start', DateTimeType::class, [
            //     'date_widget' => 'single_text',
            // ])
            // ->add('end', DateTimeType::class, [
            //     'date_widget' => 'single_text',
            // ])
            
            // ->add('all_day')
            ->add('background_color', ChoiceType::class, [
                'label' => 'Background Color',
                'choices' => [
                    'Tâche effectuée (verte)' => '#28a745',
                    'Tâche à faire (rouge)' => '#dc3545'
                    
                ],
            ])
            ->add('isValidated', ChoiceType::class, [
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
                'expanded' => true, 
                'multiple' => false, 
            ])
            ->add('description')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Calendar::class,
        ]);
    }
}
