<?php

namespace App\Form;

use App\Entity\Sexe;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Regex;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('username', null,[
            'attr' => [
                'placeholder' => 'Dupont.F'
            ],
        ])
        ->add('email', null, [
            'attr' => [
                'placeholder' => 'françois.dupont@gmail.fr'
            ],
            'constraints' => [
                new Regex('/^[a-zA-Z0-9._%+-]+@(outlook|gmail|yahoo|orange).(com|fr)$/', "L'adresse e-mail n'est pas valide")
            ]
            ])
            ->add('nom', null, [
                'attr' => [
                    'placeholder' => 'Dupont'
                ],
            'constraints' => [
                new Regex('/^[a-zA-Z]{3,}$/',
                'Minimum 3 caractères')
            ]])
            ->add('prenom', null, [
                'attr' => [
                    'placeholder' => 'François'
                ],
                'constraints' => [
                    new Regex('/^[a-zA-Z]{3,}$/',
                    'Minimum 3 caractères')
                ]
                ])
            ->add('telephone', null, [
                'attr' => [
                    'placeholder' => '00000000000'
                ],
                'constraints' => [
                    new Regex('/^(0|\+33)[1-9]([-. ]?[0-9]{2}){4}$/',
                    '+33 / 0033 / 06 ...')
                ]
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter nos conditions.',
                    ]),
                ],
            ])
            ->add('plainPassword', PasswordType::class, [
                'mapped' => false,
                'attr' => [
                'autocomplete' => 'new-password',
                'placeholder' => '••••••••'
                ],
                'constraints' => [
                    new Regex('/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/', 
                    "Il faut un mot de passe de 8 caractères avec une majuscule, une minuscule, un chiffre et un caractère spécial.")
                ],
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}