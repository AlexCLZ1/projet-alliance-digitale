**PROJET ALLIANCE DIGITAL**


Projet de formation: 
- Créer une application permettant d'affecter les tâches et de pouvoir afficher lorsqu'elles sont effectués permettant la communication entre l'équipe


**Languages/Framework utilisés**

- HTML / CSS / PHP 
- Symfony 5 / Bootstrap
- Bundle easyAdmin et fullCalendar

**Logiciels utilisés**

- Visual studio Code
- Wamp 

-------------------------------------------------------------------------------------------------------------------------
**Pour commencer à l'utiliser :**

Une fois le dossier importé,insérer les commandes suivantes dans le terminal : 

-npm install
-npm run build
-composer install

**Développement futur :**

Remettre en ordre et bien annoté le code général, créer le système de récurrence afin de pouvoir affecter une même tâche sur plusieurs jours donnés, améliorer le design et les fonctionnalités globales.

**Auteur **
@AlexCLZ1
